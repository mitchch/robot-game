# Robot Game
The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
The supported commands are listed below.

    PLACE X,Y,F  
    MOVE  
    LEFT  
    RIGHT  
    REPORT

## Installation
The application was built with node.js (using typescript). The minimum supported node version is 10.0.
You must install the node modules prior to running the application.

```javascript
npm install
```

## Starting the application
After installing node modules & setting the desired configuration, the application can be executed by running the start script.

```javascript
npm start
```

## Configuration
The application includes an environment file (.env) where some configuration can be set, such as: 

*  the input method(either CONSOLE or FILE)
*  the input file path 
*  the board size

### Console
When using the console input method, simply enter valid commands into the terminal after starting the application.

### File
The input.txt file in the root directory can be updated with the desired commands for use with the file input method.

## Tests
All tests can be executed by simply running the test script
```javascript
npm test
```