module.exports = {
  rootDir: "tests",
  preset: "ts-jest",
  testEnvironment: "node",
  setupFiles: ["./setup.ts"],
};
