import RobotGame from "../src/robot-game";

describe("tests numerous different example scripts to ensure correct output", () => {
  const consoleSpy = jest.spyOn(console, "log");

  let robotGame: RobotGame;

  beforeEach(() => {
    robotGame = new RobotGame();
  });

  test("tests the first data file to ensure the correct result", async () => {
    process.env.INPUT_FILE_PATH = "tests/test-data-1.txt";
    await robotGame.startGame();
    expect(consoleSpy).toHaveBeenCalledWith("0,1,NORTH");
  });

  test("tests the second data file to ensure the correct result", async () => {
    process.env.INPUT_FILE_PATH = "tests/test-data-2.txt";
    await robotGame.startGame();
    expect(consoleSpy).toHaveBeenCalledWith("0,0,WEST");
  });

  test("tests the third data file to ensure the correct result", async () => {
    process.env.INPUT_FILE_PATH = "tests/test-data-3.txt";
    await robotGame.startGame();
    expect(consoleSpy).toHaveBeenCalledWith("3,3,NORTH");
  });
});
