import Board from "../src/board";
import { Direction } from "../src/data/direction";

describe("tests the different possible commands", () => {
  let board: Board;

  beforeEach(() => {
    board = new Board(5);
  });

  describe("tests the place command", () => {
    test("tests an valid place command results in robot being placed", () => {
      board.place([2, 2], Direction.NORTH);
      expect(board.robot).toBeDefined();
    });

    test("tests an invalid place command results in robot not being placed", () => {
      board.place([99, 2], Direction.NORTH);
      expect(board.robot).toBeUndefined();
    });

    test("tests the position and direction are set properly when placing the robot", () => {
      board.place([0, 2], Direction.NORTH);
      expect(board.robot.position[0]).toBe(0);
      expect(board.robot.position[1]).toBe(2);
      expect(board.robot.direction).toBe(Direction.NORTH);
    });
  });

  describe("tests the move command", () => {
    test("tests a valid move north results in the robot moving to the correct position (and does not affect direction)", () => {
      board.place([2, 2], Direction.NORTH);
      board.robot.move();
      expect(board.robot.position[0]).toBe(2);
      expect(board.robot.position[1]).toBe(3);
      expect(board.robot.direction).toBe(Direction.NORTH);
    });

    test("tests a valid move east results in the robot moving to the correct position (and does not affect direction)", () => {
      board.place([2, 2], Direction.EAST);
      board.robot.move();
      expect(board.robot.position[0]).toBe(3);
      expect(board.robot.position[1]).toBe(2);
      expect(board.robot.direction).toBe(Direction.EAST);
    });

    test("tests a valid move south results in the robot moving to the correct position (and does not affect direction)", () => {
      board.place([2, 2], Direction.SOUTH);
      board.robot.move();
      expect(board.robot.position[0]).toBe(2);
      expect(board.robot.position[1]).toBe(1);
      expect(board.robot.direction).toBe(Direction.SOUTH);
    });

    test("tests a valid move west results in the robot moving to the correct position (and does not affect direction)", () => {
      board.place([2, 2], Direction.WEST);
      board.robot.move();
      expect(board.robot.position[0]).toBe(1);
      expect(board.robot.position[1]).toBe(2);
      expect(board.robot.direction).toBe(Direction.WEST);
    });

    test("tests the robot cannot move off of the table", () => {
      board.place([3, 3], Direction.NORTH);
      board.robot.move();
      board.robot.move();
      board.robot.move();
      board.robot.move();
      expect(board.robot.position[0]).toBe(3);
      expect(board.robot.position[1]).toBe(4);
    });
  });

  test("tests the left command moves the robot in the correct direction", () => {
    board.place([2, 2], Direction.NORTH);

    board.robot.left();
    expect(board.robot.direction).toBe(Direction.WEST);

    board.robot.left();
    expect(board.robot.direction).toBe(Direction.SOUTH);

    board.robot.left();
    expect(board.robot.direction).toBe(Direction.EAST);

    board.robot.left();
    expect(board.robot.direction).toBe(Direction.NORTH);
  });

  test("tests the right command moves the robot in the correct direction", () => {
    board.place([2, 2], Direction.NORTH);

    board.robot.right();
    expect(board.robot.direction).toBe(Direction.EAST);

    board.robot.right();
    expect(board.robot.direction).toBe(Direction.SOUTH);

    board.robot.right();
    expect(board.robot.direction).toBe(Direction.WEST);

    board.robot.right();
    expect(board.robot.direction).toBe(Direction.NORTH);
  });

  test("tests the report command outputs correct value", () => {
    const consoleSpy = jest.spyOn(console, "log");

    board.place([2, 2], Direction.NORTH);
    board.robot.report();

    expect(consoleSpy).toHaveBeenCalledWith("2,2,NORTH");
  });
});
