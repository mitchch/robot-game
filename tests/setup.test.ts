import RobotGame from "../src/robot-game";

describe("tests setup of robot game", () => {
  const robotGame = new RobotGame();

  beforeEach(() => {
    process.env.INPUT_METHOD = "FILE";
    process.env.INPUT_FILE_PATH = "tests/test-data-1.txt";
  });

  test("starts game with no errors", async () => {
    await expect(robotGame.startGame()).resolves.not.toThrowError();
  });

  test("reading of incorrect input method results in an error", async () => {
    process.env.INPUT_METHOD = "NOT_A_METHOD";
    await expect(robotGame.startGame()).rejects.toThrowError("Invalid input method defined");
  });

  test("reading of non-text file results in an error", async () => {
    process.env.INPUT_FILE_PATH = "tests/test-data-1.docx";
    await expect(robotGame.startGame()).rejects.toThrowError("Only .txt files are supported");
  });

  test("incorrect file path results in an error", async () => {
    process.env.INPUT_FILE_PATH = "tests/wrong-test-data-1.txt";
    await expect(robotGame.startGame()).rejects.toThrowError(`Input file at path ${process.env.INPUT_FILE_PATH} not found`);
  });
});
