import RobotGame from "../src/robot-game";
import { Action } from "../src/data/action";

describe("test robot input commands", () => {
  const robotGame = new RobotGame();

  describe("test place command", () => {
    test("test place command is valid", () => {
      expect(robotGame.isValidInput("PLACE 0,0,NORTH")).toBeTruthy();
    });

    test("test place command with no parameters command is invalid", () => {
      expect(robotGame.isValidInput("PLACE")).toBeFalsy();
    });

    test("test place command with incorrect position is invalid", () => {
      expect(robotGame.isValidInput("PLACE 0,-7,NORTH")).toBeFalsy();
    });

    test("test place command with incorrect direction is invalid", () => {
      expect(robotGame.isValidInput("PLACE 0,0,NORTHEAST")).toBeFalsy();
    });
  });

  const otherCommands: string[] = Object.values(Action).filter((x) => typeof x === "string" && x !== "PLACE");

  test("test other commands", () => {
    expect(otherCommands.every((x) => robotGame.isValidInput(x))).toBeTruthy();
  });

  describe("test other inputs are invalid", () => {
    test("test incorrect capilisation is invalid", () => {
      expect(otherCommands.every((x) => robotGame.isValidInput(x.toLowerCase()))).toBeFalsy();
    });

    test("test extra paramaters is invalid", () => {
      expect(otherCommands.every((x) => robotGame.isValidInput(x + ", 0"))).toBeFalsy();
    });

    test("test extra characters after command is invalid", () => {
      expect(otherCommands.every((x) => robotGame.isValidInput(x + "E"))).toBeFalsy();
    });

    test("test extra characters before command is invalid", () => {
      expect(otherCommands.every((x) => robotGame.isValidInput("M" + x))).toBeFalsy();
    });
  });
});
