import readline = require("readline");
import fs = require("fs");

import Board from "./board";
import { Action } from "./data/action";
import { Direction } from "./data/direction";
import { InputMethod } from "./data/input-method";

export default class RobotGame {
  readonly board: Board;

  constructor() {
    const boardSize = Number(process.env.BOARD_SIZE);

    if (Number.isNaN(boardSize) || boardSize < 1) {
      throw new Error("Board size must be a valid number above 0");
    }

    this.board = new Board(Number(process.env.BOARD_SIZE));
  }

  private async checkFileExists(filePath: string): Promise<boolean> {
    return fs.promises
      .access(filePath, fs.constants.F_OK)
      .then(() => true)
      .catch(() => false);
  }

  private async onInputFromFile() {
    const filePath = process.env.INPUT_FILE_PATH;

    if (filePath === undefined) {
      return Promise.reject(new Error("Input file not specified"));
    }

    if (filePath.substr(filePath.lastIndexOf(".") + 1) !== "txt") {
      return Promise.reject(new Error("Only .txt files are supported"));
    }

    if (!(await this.checkFileExists(filePath))) {
      return Promise.reject(
        new Error(`Input file at path ${filePath} not found`)
      );
    }

    const rl = readline.createInterface({
      input: fs.createReadStream(filePath),
    });

    const readFileLines = async () => {
      for await (const line of rl) {
        this.handleInput(line);
      }
    };

    await readFileLines();
  }

  private onInputFromConsole() {
    const rl = readline.createInterface({ input: process.stdin });

    const getInputFromConsole = () => {
      rl.question("", (input) => {
        this.handleInput(input);
        getInputFromConsole();
      });
    };

    getInputFromConsole();
  }

  isValidInput(input: string): boolean {
    return new RegExp(`^(PLACE [0-9]+,[0-9]+,(NORTH|EAST|SOUTH|WEST)|MOVE|LEFT|RIGHT|REPORT)$`).test(input);
  }

  private handleInput(input: string): void {
    if (!this.isValidInput(input)) {
      return;
    }

    const splitted = input.split(/,| /);

    const action: Action = Action[splitted[0] as keyof typeof Action];

    switch (action) {
      case Action.PLACE:
        return this.board.place([Number(splitted[1]), Number(splitted[2])], Direction[splitted[3] as keyof typeof Direction]);

      case Action.MOVE:
        return this.board.robot?.move();

      case Action.LEFT:
        return this.board.robot?.left();

      case Action.RIGHT:
        return this.board.robot?.right();

      case Action.REPORT:
        return this.board.robot?.report();
    }
  }

  async startGame(): Promise<void> {
    const inputMethod = InputMethod[process.env.INPUT_METHOD as keyof typeof InputMethod];

    if (inputMethod === undefined) {
      return Promise.reject(new Error("Invalid input method defined"));
    }

    switch (inputMethod) {
      case InputMethod.FILE: {
        return this.onInputFromFile();
      }

      case InputMethod.CONSOLE: {
        return this.onInputFromConsole();
      }
    }
  }
}
