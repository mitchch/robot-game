import dotenv = require("dotenv");
import RobotGame from "./robot-game";

const init = () => {
  dotenv.config();

  const robotGame = new RobotGame();

  robotGame.startGame();
};

init();
