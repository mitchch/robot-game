import { Direction } from "./data/direction";
import Board from "./board";

export default class Robot {
  readonly board: Board;

  private _position: [number, number];
  private _direction: Direction;

  constructor(board: Board) {
    this.board = board;
  }

  set position(position: [number, number]) {
    if (this.board.isValidPosition(position)) {
      this._position = position;
    }
  }

  get position(): [number, number] {
    return this._position;
  }

  set direction(direction: Direction) {
    this._direction = direction;
  }

  get direction(): Direction {
    return this._direction;
  }

  private getNextTurnDirection(turningRight: boolean): Direction {
    switch (this.direction) {
      case Direction.NORTH:
        return turningRight ? Direction.EAST : Direction.WEST;
      case Direction.EAST:
        return turningRight ? Direction.SOUTH : Direction.NORTH;
      case Direction.SOUTH:
        return turningRight ? Direction.WEST : Direction.EAST;
      case Direction.WEST:
        return turningRight ? Direction.NORTH : Direction.SOUTH;
    }
  }

  private getNextMovePosition(): [number, number] {
    const [xPosition, yPosition] = this.position;

    switch (this.direction) {
      case Direction.NORTH:
        return [xPosition, yPosition + 1];
      case Direction.EAST:
        return [xPosition + 1, yPosition];
      case Direction.SOUTH:
        return [xPosition, yPosition - 1];
      case Direction.WEST:
        return [xPosition - 1, yPosition];
    }
  }

  move(): void {
    this.position = this.getNextMovePosition();
  }

  left(): void {
    this.direction = this.getNextTurnDirection(false);
  }

  right(): void {
    this.direction = this.getNextTurnDirection(true);
  }

  report(): void {
    console.log(`${this.position[0]},${this.position[1]},${this.direction}`);
  }
}
