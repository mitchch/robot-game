import Robot from "./robot";
import { Direction } from "./data/direction";

export default class Board {
  private readonly boardSize: number;
  private _robot: Robot;

  constructor(boardSize: number) {
    this.boardSize = boardSize;
  }

  set robot(robot: Robot) {
    this._robot = robot;
  }

  get robot(): Robot {
    return this._robot;
  }

  place(position: [number, number], direction: Direction): void {
    if (!this.isValidPosition(position)) {
      return;
    }

    this.robot = this.robot ?? new Robot(this);
    this.robot.position = position;
    this.robot.direction = direction;
  }

  isValidPosition(position: [number, number]): boolean {
    return position.every((x) => x >= 0 && x < this.boardSize);
  }
}
